
public class Main {
    public static void main(String[] args) {

        int numbers []= {1,2,3};
        try {
            System.out.println(numbers [0]);
            System.out.println (numbers [1]);
            System.out.println(numbers [2]);
            System.out.println (numbers [3]);
        }
        catch (Exception e) {
        System.out.println("This is why QA Engineers always have to do boundary testing! The array only has 3 values and you've requested a 4th");
        }

    }
}
